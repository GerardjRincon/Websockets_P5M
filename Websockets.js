var FancyWebSocket = function(url)
{
	var callbacks = {};
	var ws_url = url;
	var conn;
	
	this.bind = function(event_name, callback)
	{
		callbacks[event_name] = callbacks[event_name] || [];
		callbacks[event_name].push(callback);
		return this;
	};
	
	this.send = function(event_name, event_data)
	{
		this.conn.send( event_data );
		return this;
	};
	
	this.connect = function() 
	{
		if ( typeof(MozWebSocket) == 'function' )
		this.conn = new MozWebSocket(url);
		else
		this.conn = new WebSocket(url);
		
		this.conn.onmessage = function(evt)
		{
			dispatch('message', evt.data);
		};
		
		this.conn.onclose = function(){dispatch('close',null)}
		this.conn.onopen = function(){dispatch('open',null)}
	};
	
	this.disconnect = function()
	{
		this.conn.close();
	};
	
	var dispatch = function(event_name, message)
	{
		if(message == null || message == "")//aqui es donde se realiza toda la accion
			{
			}
			else{
				var msg    = JSON.parse(message); //parseo la informacion
				switch(msg.success)//que tipo de actualizacion vamos a hacer(un nuevo mensaje, solicitud de amistad nueva, etc )
				{
					case '1':
					if (msg.Autoridad == '2') {
						AlertaNoti(msg);
					}else{
					noti(msg);
					}
					break;
					case '0':
					actualiza_solicitud(msg.mensaje);
					break;
					
				}	
			}
	var chain = callbacks[event_name];
		if(typeof chain == 'undefined') return; // no callbacks for this event
		for(var i = 0; i < chain.length; i++){
			chain[i]( message )
		}
	}

};

var Server;
function log( text ) {
			
			console.log(text);
			//Autoscroll
		}
function send( text ) 
{
   Server.send( 'message', text );
}
$(document).ready(function() 
{
	log("Conectando el socket");
	Server = new FancyWebSocket('ws://localhost:8080');
    Server.bind('open', function()
	{
		log("Conectado");
    });
    Server.bind('close', function( data ) 
	{
		log("Desconectado");
    });
    Server.bind('message', function( payload ) 
	{
    });
    Server.connect();
});



// function actualiza_mensaje(message)
// {
// 	 //parseo la informacion
// 				var tipo = message[0].success;
// 				var mensaje = message[0].mensaje;
// 				console.log(tipo+' - '+mensaje);
// }
function noti(message)
{
	$('#alerta_noti').html(message.success);
	new PNotify({
        title: 'Notificación',
        text: 'Tienes '+message.success+' notificaciónes nuevas',
        type: 'success'
    });
	
	
	
}
function AlertaNoti(message) {
    stack_context = {
            "dir1": "down",
            "dir2": "left",
            "context": $("#bodyL")
        };
	switch(message.Accion)
	{
		// alerta de guardado en caso que sea el apostol
		case 'EventoGuard':
			var notiL= $('#alerta_L').html();
			var suma = parseInt(notiL) + parseInt(message.success);
			$('#alerta_L').html(suma);
			$('#agenMini').fullCalendar('refetchEvents');
				new PNotify({
		        title: 'Evento guardado',
		        text: message.Evento,
		        type: 'success',
		        addclass: "stack-bottomleft",
		        stack: stack_context
		    	});
		break;
		
		// aleta cuando el apostol mueva un evento de fecha
		case 'EventoModifyD':
			var notiB= $('#Bandera_L').html();
			if(notiB){
			var sumaB = parseInt(notiB) + parseInt(message.success);
			$('#Bandera_L').html(sumaB);
			$('#agenMini').fullCalendar('refetchEvents');
				new PNotify({
		        title: 'Evento movido',
		        text: message.Evento,
		        type: 'success',
		        addclass: "stack-bottomleft",
		        stack: stack_context
		    	});
		    }else{
		    $('#Bandera_L').html(message.success);
			$('#agenMini').fullCalendar('refetchEvents');
				new PNotify({
		        title: 'Evento movido',
		        text: message.Evento,
		        type: 'success',
		        addclass: "stack-bottomleft",
		        stack: stack_context
		    	});
		    }
		break;

		// Alerta cuando el apostol elimina un evento
		case 'EventoElim':
		var notiC= $('#Bandera_L').html();
			if(notiC){
			var sumaC = parseInt(notiC) + parseInt(message.success);
			$('#Bandera_L').html(sumaC);
			$('#agenMini').fullCalendar('refetchEvents');
				new PNotify({
		        title: 'Evento movido',
		        text: message.Evento,
		        type: 'success',
		        addclass: "stack-bottomleft",
		        stack: stack_context
		    	});
		    }else{
		    $('#Bandera_L').html(message.success);
			$('#agenMini').fullCalendar('refetchEvents');
				new PNotify({
		        title: 'Evento movido',
		        text: message.Evento,
		        type: 'success',
		        addclass: "stack-bottomleft",
		        stack: stack_context
		    	});
		    }
		break;
	}
	
}
function actualiza_solicitud(message)
{
	console.log(message);
}
function error_mensaje(){
	new PNotify({
        title: 'Error!',
        text: 'La accion que ejecutaste es nula, por favor intenta nuevamente.',
        type: 'error'
    });
}
